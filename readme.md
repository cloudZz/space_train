# Space Train Crash
A highly sophisticated Interplanetarian Railway Simulator. Made for Global Game Jam 2019.
Checkout the tag `ggj19` to see the state of the code at jam end.
If you want an actual playable level, you can checkout `ggj19-playable` for that (made after the jam).
`master` contains a slightly updated post-jam version with 3 playable levels and a win and a game over screen.

Help trains find their way back to their home planet!

Used:
* Python & Kivy
* the Tiled mapeditor for the levels
* Train sound effect: http://soundbible.com
* Bfxr for other sound effects
* GraphicsGale

## Releases
[Windows binary](https://ln.sync.com/dl/06a8509e0/siugxq5v-ajdsd2cx-5n4zvmtk-ep5ajxdt)
