from enum import Enum
import random
import tmx
import os
from widgets import Tile, ToggleImage, TileTogglerWidget
#import pytmx

class OutOfGrid(Exception):
    pass

# rail orientation
class Orientation(Enum):
    LOWER_LEFT = 0
    LOWER_RIGHT = 1
    UPPER_LEFT = 2
    UPPER_RIGHT = 3
    HORIZ = 4
    VERT = 5

# toggle orientation
class ToggleState(Enum):
    UP = 0
    DOWN = 1
    LEFT = 2
    RIGHT = 3


def make_trail_tile(gid):
    if gid == 26:
        return CurveRail(Orientation.UPPER_LEFT)
    elif gid== 27:
        return HorizRail()
    if gid == 28:
        return CurveRail(Orientation.UPPER_RIGHT)
    elif gid== 31:
        return VertRail()
    elif gid== 35:
        return TrainStation()
    elif gid== 36:
        return CurveRail(Orientation.LOWER_LEFT)
    elif gid== 38:
        return CurveRail(Orientation.LOWER_RIGHT)
    
    else:
        return NullTile()

def make_toggle_tile(gid, tilemap, own_x, own_y):
    if gid == 34:
        return VertRailToggle(ToggleState.LEFT, tilemap, own_x, own_y)
    elif gid == 29:
        return VertRailToggle(ToggleState.RIGHT, tilemap, own_x, own_y)
    elif gid == 24:
        return HorizRailToggle(ToggleState.UP, tilemap, own_x, own_y)
    elif gid == 19:
        return HorizRailToggle(ToggleState.DOWN, tilemap, own_x, own_y)  
    else:
        return None

class VertRail(Tile):
    def orient(self):
        return Orientation.VERT

    def print(self):
        print('vert rail.')
    def short(self):
        return '|' 

    def filename(self):
        return '031.png'

class HorizRail(Tile):
    def orient(self):
        return Orientation.HORIZ
    def print(self):
        print('horiz rail.')
    def short(self):
        return '-'
    def filename(self):
        return '027.png'

class TrainStation(Tile):
    def __init__(self):
        super(TrainStation, self).__init__()
        self.color = None
    
    def print(self):
        print('train station.')
    def short(self):
        return 'x'
    def filename(self):
        return '035.png'

class CurveRail(Tile):
    def __init__(self, orientation):
        self.orientation = orientation
        super(CurveRail, self).__init__()

    # TODO: replace direct variable access with this
    def orient(self):
        return self.orientation
    def print(self):
        print('curve rail, orientation ' + str(self.orientation))

    def short(self):
        if self.orientation == Orientation.LOWER_LEFT:
            return '\\'
        elif self.orientation == Orientation.LOWER_RIGHT:              
            return '/'
        elif self.orientation == Orientation.UPPER_LEFT:              
            return '/'
        elif self.orientation == Orientation.UPPER_RIGHT:              
            return '\\'
    def filename(self):
        if self.orientation == Orientation.LOWER_LEFT:
            return '036.png'
        elif self.orientation == Orientation.LOWER_RIGHT:              
            return '038.png'
        elif self.orientation == Orientation.UPPER_LEFT:              
            return '026.png'
        elif self.orientation == Orientation.UPPER_RIGHT:              
            return '028.png'
    def mirror(self, how):
        assert(how == "h" or how == "v")
        if how == "h": # horrizontal
            if self.orientation == Orientation.LOWER_LEFT:
                self.orientation = Orientation.UPPER_LEFT
            elif self.orientation == Orientation.LOWER_RIGHT:              
                self.orientation = Orientation.UPPER_RIGHT
            elif self.orientation == Orientation.UPPER_LEFT:              
                self.orientation = Orientation.LOWER_LEFT
            elif self.orientation == Orientation.UPPER_RIGHT:              
                self.orientation = Orientation.LOWER_RIGHT
        else: # vertical
            if self.orientation == Orientation.LOWER_LEFT:
                self.orientation = Orientation.LOWER_RIGHT
            elif self.orientation == Orientation.LOWER_RIGHT:              
                self.orientation = Orientation.LOWER_LEFT
            elif self.orientation == Orientation.UPPER_LEFT:              
                self.orientation = Orientation.UPPER_RIGHT
            elif self.orientation == Orientation.UPPER_RIGHT:              
                self.orientation = Orientation.UPPER_LEFT

class NullTile(Tile):
    def short(self):
        return ' '
    def filename(self):
        return 'empty.png'

def toggleCallbackOn():
    print("toggle callback on")
def toggleCallbackOff():
    print("toggle callback off")

class HorizRailToggle(Tile):
    def __init__(self, toggle_status, tilemap, own_x, own_y):
        self.state = toggle_status
        super(HorizRailToggle, self).__init__()
        self.widget = TileTogglerWidget(os.path.join("../tiles/", self._pic()), os.path.join("../tiles/", self._pic(opposite=True)), tilemap, own_x, own_y, "h")
    def _init_widget(self):
        #return ToggleImage(os.path.join("../tiles/", self._pic()), os.path.join("../tiles/", self._pic(opposite=True)), toggleCallbackOff, toggleCallbackOn)
        pass
    def _pic(self, opposite=False):
        if (self.state == ToggleState.UP and not opposite) or (self.state == ToggleState.DOWN and opposite):
            return '024.png'
        elif (self.state == ToggleState.DOWN and not opposite) or (self.state == ToggleState.UP and opposite):
            return '019.png'
    def short(self):
        if self.state == ToggleState.UP:
            return 'U'
        elif self.state == ToggleState.DOWN:
            return 'D'
    def filename(self):
        if self.state == ToggleState.UP:
            return '024.png'
        elif self.state == ToggleState.DOWN:
            return '019.png'
class VertRailToggle(Tile):
    def __init__(self, toggle_status, tilemap, own_x, own_y):
        self.state = toggle_status
        super(VertRailToggle, self).__init__()
        self.widget = TileTogglerWidget(os.path.join("../tiles/", self._pic()), os.path.join("../tiles/", self._pic(opposite=True)), tilemap, own_x, own_y, "v")
    def _init_widget(self):
        #return ToggleImage(os.path.join("../tiles/", self._pic()), os.path.join("../tiles/", self._pic(opposite=True)), toggleCallbackOff, toggleCallbackOn)
        pass
    def _pic(self, opposite=False):
        if (self.state == ToggleState.LEFT and not opposite) or (self.state == ToggleState.RIGHT and opposite):
            return '034.png'
        elif (self.state == ToggleState.RIGHT and not opposite) or (self.state == ToggleState.LEFT and opposite):
            return '029.png'
    def short(self):
        if self.state == ToggleState.LEFT:
            return 'L'
        elif self.state == ToggleState.RIGHT:
            return 'R'
    def filename(self):
        if self.state == ToggleState.LEFT:
            return '034.png'
        elif self.state == ToggleState.RIGHT:
            return '029.png'



class Map:
    def __init__(self, width, height, callback=None, tmxfile=None):
        self.tiles_linear = [0] * width * height
        self.width = width
        self.height = height

        if tmxfile and callback:
            tmxdata = tmx.TileMap.load(tmxfile)
            for layer in tmxdata.layers:
                if layer.name == "trails":
                    for i, t in enumerate(layer.tiles): #first pass: rails
                        self.tiles_linear[i] = callback(t.gid-1) #gid minus 1 because gid 0 is None
                    for i, t in enumerate(layer.tiles): #2nd pass: toggles
                        x = i % tmxdata.width
                        y = i // tmxdata.width
                        potential_toggle_tile = make_toggle_tile(t.gid-1, self, x, y)
                        if potential_toggle_tile is not None:
                            self.tiles_linear[i] = potential_toggle_tile
#                if layer.name == "toggles": #TODO assumes trails was done first!
#                    print("toggles")
#                    for obj in layer.objects:
#                        x = obj.x//obj.width # convert pixel coords to tile coords
#                        y = (obj.y//obj.height) - 1 # FIXME weird Tiled behaviour
#                        print(x, y)
#                        #print(obj.gid, obj.properties[0].value, obj.properties[1].value)
#                        curve_x = None
#                        curve_y = None
#                        for p in obj.properties: 
#                            if p.name == "curve_x":
#                                curve_x = int(p.value)
#                            elif p.name == "curve_y":
#                                curve_y = int(p.value)
#                        print(curve_x, curve_y)
#                        curveRailTile = self.get_at(curve_x, curve_y)
#                        self.tiles_linear[x + y * self.width] = make_toggle_tile(obj.gid-1, curveRailTile)

        else:
            for x in range(width):
                for y in range(height):
                    #print(x, y)
                    rand = random.randint(0, 3)
                    if rand <= 1:
                        self.tiles_linear.append(HorizRail())
                    elif rand <= 2:
                        self.tiles_linear.append(VertRail())
                    else:
                        self.tiles_linear.append(CurveRail(Orientation.LOWER_LEFT))                    

    def set_train_station_colors(self, color_list):
        colors = iter(color_list)
        for tile in self.tiles_linear:
            if tile.__class__ == TrainStation:
                tile.color = next(colors)

    def get_at(self, x, y):
        index = x + y * self.width
        if index < 0 or index >= len(self.tiles_linear):
            raise OutOfGrid('out of grid!')
        return self.tiles_linear[index]

def predicate_fill(tmxfile, tile_size, layer_name, predicate):
    tmxdata = tmx.TileMap.load(tmxfile)
    for layer in tmxdata.layers:
        if layer.name == layer_name:
            coords = []
            for i, t in enumerate(layer.tiles):
                gid = t.gid - 1 # since gid 0 is one -> offset
                if predicate(gid):
                    x = i % tmxdata.width
                    y = i // tmxdata.width
                    #print(x, y)
                    #coords.append((gid, int((x + .5) * tile_size), int((y + .5) * tile_size)))
                    coords.append((gid, x, y, int((x + .5) * tile_size) - tmxdata.width * tile_size / 2, -int((y + .5) * tile_size) + tmxdata.height * tile_size / 2))
                    #coords.append((gid, int(x * tile_size), int(y * tile_size)))
    return coords

class PlanetField:
    def __init__(self, tmxfile, tile_size):
        # the middle
        self.coords = [(x, y) for _, _, _, x, y in predicate_fill(tmxfile, tile_size, 'decoration', lambda gid: gid == 10)]

class TrainStarts:
    def __init__(self, tmxfile, tile_size):
        all_coords = predicate_fill(tmxfile, tile_size, 'train_starts', lambda gid: gid in [10, 11, 12, 15, 16, 17]) # the world tiles
        #self.coords = predicate_fill(tmxfile, tile_size, 'train_starts', lambda gid: print(gid))
        # sort according to intended train number
        sorted_coords = sorted(all_coords)
        self.coords = [(x, y) for _, _, _, x, y in sorted_coords]
        self.grid_coords = [(x, y) for _, x, y, _, _ in sorted_coords]

def main():
    WIDTH = 64
    HEIGHT = 32

    #map = Map(WIDTH, HEIGHT)
    #map.get_at(1, 0).print()

    #for x in range(WIDTH):
    #        print('   '.join([map.get_at(x, y).short() for y in range(HEIGHT)]))

    #tmxdata = pytmx.TiledMap("../test.tmx")
    #iter_data = tmxdata.get_layer_by_name('trails').iter_data()
    #print(list(iter_data)[9 + 7 * 30]) 
    #print([gid for x, y, gid in iter_data]) 
    #print(tmxdata.get_tile_properties_by_gid(28)) 
    #print(tmxdata.get_tile_properties(9, 7, 0))

    map2 = Map(30, 20, make_trail_tile, '../test.tmx')
    for y in range(20):
            print('   '.join([map2.get_at(x, y).short() for x in range(30)]))

    planet_field = PlanetField('../test.tmx', 32)
    print(planet_field.coords)

if __name__ == '__main__':
    main()

