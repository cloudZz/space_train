from kivy.uix.image import Image
from kivy.uix.behaviors import ToggleButtonBehavior
from kivy.uix.label import Label
from kivy.uix.gridlayout import GridLayout
#from data_structure import OutOfGrid
import data_structure
import os
from main import play_sound

from main import TILE_SIZE
class Tile:
    def __init__(self):
        self.widget = self._init_widget()
    def _init_widget(self):
        img = Image(source=os.path.join('../tiles/', self.filename()))
        img.size_hint_x = None
        img.size_hint_y = None
        img.width = TILE_SIZE
        img.height = TILE_SIZE
        return img
    def filename(self):
        pass

# toggleable button that changes a curve tile
class TileTogglerWidget(ToggleButtonBehavior, Image):
    def __init__(self, img_off, img_on, tilemap, own_x, own_y, horizontal_or_vertical, **kwargs):
        super(TileTogglerWidget, self).__init__(**kwargs)
        self.source = img_off
        self.img_off = img_off
        self.img_on = img_on
        self.horizontal_or_vertical = horizontal_or_vertical
        self.tilemap = tilemap
        self.tile_x = own_x
        self.tile_y = own_y
        print("toggle:", own_x, own_y)
    def on_state(self, widget, value):
        play_sound("push_toggle.wav")
        if value == 'down':
            self.source = self.img_on     
        else:
            self.source = self.img_off
        if self.horizontal_or_vertical == "v":
            self.try_mirror(0, 1)    
            self.try_mirror(0, -1)
        else:
            self.try_mirror(1, 0)
            self.try_mirror(-1, 0)

        #self.curveRail.mirror(self.horizontal_or_vertical)
        # update image to flipped one
        #self.curveRail.widget.source = os.path.join('../tiles/', self.curveRail.filename())
        #self.curveRail.widget.texture.flip_vertical() #does not work?
    def try_mirror(self, offset_x, offset_y):
        try:
            tile = self.tilemap.get_at(self.tile_x + offset_x, self.tile_y + offset_y)
            #print(self.horizontal_or_vertical, type(tile))    
            if type(tile) == data_structure.CurveRail:
                tile.mirror(self.horizontal_or_vertical)
                tile.widget.source = os.path.join('../tiles/', tile.filename()) 
        except data_structure.OutOfGrid: #FIXME cannot import OutOfGrid circular dependency
            print("cannot get tile at", self.tile_x + offset_x, self.tile_y + offset_y)
            pass

class RailBuilderToggle(ToggleButtonBehavior, Image):
    def __init__(self, tool_count, img_off, img_on, callback_off=None, callback_on=None,**kwargs):
        super(RailBuilderToggle, self).__init__(**kwargs)
        self.source = img_off
        self.img_off = img_off
        self.img_on = img_on
        self.tool_count = tool_count
        self.callback_on = callback_on
        self.callback_off = callback_off


    def on_state(self, widget, value):
        if value == 'down':
            if self.tool_count.avail_tools > 0:
                self.source = self.img_on
                #self.callback_on()
            else:
                # error not enough money left
                pass      
        else:
            self.source = self.img_off
            #self.callback_off()
class ToolCountLabel(Label):
    def __init__(self, start_tool_quota, **kwargs):
        self.avail_tools = start_tool_quota
        super(ToolCountLabel, self).__init__(**kwargs)
        self._update()
    def _update(self):
        self.text = str(self.avail_tools)
    def inc(self):
        self.avail_tools += 1
        self._update()
    def dec(self):
        self.avail_tools -= 1
        self._update()

class BuilderTools(GridLayout):
    def __init__(self, start_tool_quota, **kwargs):
        super(BuilderTools, self).__init__(**kwargs)
        self.tool_count = ToolCountLabel(start_tool_quota)
        #horiz_toggle = RailBuilderToggle #TODO
        #vert_toggle = RailBuilderToggle
        #curve_rail
        self.builder_toggles = [] #railbuildertoggles

def get_builder_gfx(tool_type):
    pass #todo return button gfx off and on

# toggle button with image and callback
class ToggleImage(ToggleButtonBehavior, Image):
    def __init__(self, img_off, img_on, callback_off=None, callback_on=None,**kwargs):
        super(ToggleImage, self).__init__(**kwargs)
        self.source = img_off
        self.img_off = img_off
        self.img_on = img_on
        self.callback_on = callback_on
        self.callback_off = callback_off

    def on_state(self, widget, value):
        if value == 'down':
            self.source = self.img_on
            self.callback_on()      
        else:
            self.source = self.img_off
            self.callback_off()

