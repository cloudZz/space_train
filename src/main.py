#!/usr/bin/env python3
import kivy
kivy.require('1.10.1')

from kivy.config import Config

from kivy.app import App
from kivy.uix.screenmanager import ScreenManager, Screen, FadeTransition
from kivy.uix.gridlayout import GridLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput
from kivy.uix.image import Image
from kivy.graphics import *
from kivy.animation import Animation
from kivy.clock import Clock
from kivy.core.audio import SoundLoader

import os
import time
from enum import Enum

import data_structure

WIDTH = 30
HEIGHT = 20
TILE_SIZE = 32
SPEED = 1

class GameGrid(GridLayout):

    def __init__(self, tile_map, **kwargs):
        super(GameGrid, self).__init__(**kwargs)
        self.cols = tile_map.width
        self.rows = tile_map.height

        #self.height = self.minimum_height
        #self.size_hint_y = None

        for y in range(tile_map.height):
            for x in range(tile_map.width):
                tile = tile_map.get_at(x, y)
                self.add_widget(tile.widget)
                #if type(tile) == data_structure.RailToggle:
                #    self.add_widget(tile.widget())
                #else:
                #    img = Image(source=os.path.join('../tiles/', tile.filename()))
                #    img.size_hint_x = None
                #    img.size_hint_y = None
                #    img.width = TILE_SIZE
                #    img.height = TILE_SIZE
                #    self.add_widget(img)

        #self.add_widget(Label(text='User Name'))
        #self.username = TextInput(multiline=False)
        #self.add_widget(self.username)
        #self.add_widget(Label(text='password'))
        #self.password = TextInput(password=True, multiline=False)
        #self.add_widget(self.password)

class OwnFloatLayout(FloatLayout):
    def __init__(self, **kwargs):
        super(OwnFloatLayout, self).__init__(**kwargs)
        with self.canvas.before:
            Rectangle(pos=self.pos, size=(WIDTH*TILE_SIZE, HEIGHT*TILE_SIZE), source='../background.png')

class TrainAnim(Animation):
    def __init__(self, anim_callback, delete_callback, collision_callback, **kwargs):
        super(TrainAnim, self).__init__(**kwargs)
        self.complete = False
        self.anim_callback = anim_callback
        self.delete_callback = delete_callback
        self.collision_callback = collision_callback

    def on_start(self, animation):
        self.complete = False
    def on_complete(self, animation):
        #print('complete')
        grid_x = to_grid(animation.x, WIDTH)
        grid_y = to_grid(animation.y, HEIGHT, -1)
        try:
            deleted_trains = self.collision_callback(grid_x, grid_y)
        except data_structure.OutOfGrid:
            game_over()
            return

        self.delete_callback(deleted_trains)
        self.complete = True
        self.anim_callback()

class TrainOrientation(Enum):
    HORIZ = 0,
    VERT = 1,
    LEFT_DOWN_RIGHT_UP = 2,
    RIGHT_DOWN_LEFT_UP = 3,



def orientation_predicate(tile_map, grid_x, grid_y, on_success, on_failure):
    orientation_map = {
        data_structure.HorizRail:   ('008.png', '008.png'),
        data_structure.VertRail:    ('003.png', '004.png'),
        data_structure.CurveRail:   ('013.png', '014.png'),
    }
    try:
        map_obj = tile_map.get_at(grid_x, grid_y)
    except data_structure.OutOfGrid:
        game_over()

    klass = map_obj.__class__
    # TODO: diff directions
    direction = 0, 0

    #print(grid_x, grid_y, klass)
    if klass in orientation_map:
         # TODO: Mirror based on map_obj
         return on_success(orientation_map[klass][direction[1]], map_obj, direction)
    else:
        return on_failure(map_obj, direction)

def train_in_space(klass, direction):
    raise Exception('train in space!')
    # Dummydings um Exc zu vermeiden
    #return '../tiles/022.png', klass, direction

def play_sound(name, loop=False):
    sound = SoundLoader.load(os.path.join('../sounds/', name))
    if sound:
        #print("Sound found at %s" % sound.source)
        #print("Sound is %.3f seconds" % sound.length)
        sound.loop = loop
        sound.play()
        return sound
def stop_sound(sound):
    if sound:
        sound.stop()

def movement_vector(map_obj, direction):
    orientation_map = {
        data_structure.HorizRail:   ((-1, 0), (1, 0)),
        data_structure.VertRail:    ((0, -1), (0, 1)),
        data_structure.CurveRail:   (None, None),
        data_structure.NullTile:    (None, None),
    }
    klass = map_obj.__class__
    if klass in orientation_map:
        if klass is data_structure.HorizRail or klass is data_structure.VertRail:
            #print(' not curve rail and not null')
            return orientation_map[klass][direction[1]]
        elif klass is data_structure.NullTile:
            play_sound("train_gets_offrail.wav")
            if direction[0] == 0:
                # horiz
                return (-1 if direction[1] == 0 else 1, 0)
            else:
                # vert
                return (0, -1 if direction[1] == 0 else 1)
        else:
            #print(' curve rail', map_obj.orientation)
            if map_obj.orientation is data_structure.Orientation.LOWER_LEFT:
                #return ((-1, -1), (+1, +1))[direction]
                return ((0, 1), (1, 0))[direction[0]]
            elif map_obj.orientation is data_structure.Orientation.LOWER_RIGHT:              
                #return ((-1, +1), (+1, -1))[direction]
                return ((0, 1), (-1, 0))[direction[0]]
            elif map_obj.orientation is data_structure.Orientation.UPPER_LEFT:              
                #return ((+1, +1), (-1, -1))[direction]
                return ((0, -1), (+1, 0))[direction[0]]
            elif map_obj.orientation is data_structure.Orientation.UPPER_RIGHT:              
                #return ((+1, -1), (-1, +1))[direction]
                return ((0, -1), (-1, 0))[direction[0]]
            else:
                print('BUG!')
    else:
        print(' (BUG!)')
        return None, None

def vector_convert(train, vector_x, vector_y):
    vector_x, vector_y = tuple(a * TILE_SIZE for a in (vector_x, vector_y))
    vector_x += train.pos[0]
    vector_y += train.pos[1]

    return vector_x, vector_y

# returns trains to delete
def collision_check(trains, tile_map, grid_x, grid_y):
    to_delete = []
    
    # train collisions
    for train, _, _ in trains:
        for train2, _, _ in trains:
            #print('POS:', train.pos[0], train2.pos[0], ' | ', train.pos[1], train2.pos[1])
            if train != train2 \
            and abs(train.pos[0] - train2.pos[0]) < TILE_SIZE \
            and abs(train.pos[1] - train2.pos[1]) < TILE_SIZE:
                to_delete.append(train)
    
    for train, direction, _ in trains:
        grid_x_check = to_grid(train.pos[0], WIDTH)
        grid_y_check = to_grid(train.pos[1], HEIGHT, -1)

        if grid_x_check != grid_x or grid_y_check != grid_y:
            # not our train
            continue

        try:
            tile = tile_map.get_at(grid_x, grid_y)
        except data_structure.OutOfGrid:
            game_over()
            return []

        if tile.__class__ not in [data_structure.TrainStation, data_structure.NullTile]:
            #print('orient', direction[0], 'dir', direction[1])
            if direction[0] == 0:
                if direction[1] == 0:
                    if tile.orient() in [data_structure.Orientation.LOWER_RIGHT, data_structure.Orientation.UPPER_RIGHT, data_structure.Orientation.VERT]:
                        to_delete.append(train)
                else:
                    if tile.orient() in [data_structure.Orientation.LOWER_LEFT, data_structure.Orientation.UPPER_LEFT, data_structure.Orientation.VERT]:
                        to_delete.append(train)
            else:
                #print('orient 1')
                #exit(0)
                if direction[1] == 0:
                    if tile.orient() in [data_structure.Orientation.UPPER_RIGHT, data_structure.Orientation.UPPER_LEFT, data_structure.Orientation.HORIZ]:
                        to_delete.append(train)
                else:
                    if tile.orient() in [data_structure.Orientation.LOWER_LEFT, data_structure.Orientation.LOWER_RIGHT, data_structure.Orientation.HORIZ]:
                        to_delete.append(train)
    # if any train explodes, it's game over
    if to_delete:
        play_sound("explosion.wav")
        Clock.schedule_once(lambda x: game_over(), 2)

    # Now the non-lethal stuff (train stations)
    for train, _, _ in trains:
        grid_x_check = to_grid(train.pos[0], WIDTH)
        grid_y_check = to_grid(train.pos[1], HEIGHT, -1)

        if grid_x_check != grid_x or grid_y_check != grid_y:
            # not our train
            continue

        coords = [(grid_x + x, grid_y + y) for x, y in [(-1, 0), (1, 0), (0, -1), (0, 1)]]
        #print('train', train.color, 'arriving?', coords, ' | ', grid_x, grid_y)
        tiles = [tile_map.get_at(x, y) for x, y in coords]
        #print(tiles)
        klasses = [tile.__class__ for tile in tiles]
        for tile, klass in zip(tiles, klasses):
            #if klass == data_structure.TrainStation:
                #print(tile.color)
            if klass == data_structure.TrainStation and tile.color == tuple(train.color):
                #print('train', train.color, 'arrived')
                # yeah!
                play_sound("train_station.wav")

                to_delete.append(train)
                
                # last train
                if len(trains) <= 1:
                    win()
    
    return to_delete

def to_grid(coord, dimension, factor=1):
    return int((coord + factor * dimension * TILE_SIZE / 2) / TILE_SIZE) * factor

class Trains:
    def __init__(self, layout, tile_map):
        self.trains = []
        self.tile_map = tile_map
        self.layout = layout

        self.animation_map = {}

    def add(self, color, x, y, grid_x, grid_y):
        source, _, direction = orientation_predicate(self.tile_map, grid_x, grid_y, lambda x, map_obj, direction: (os.path.join('../tiles/', x), map_obj, direction), train_in_space)
        #print('SOURCE', source)
        #print(x, grid_x * 32, grid_x)
        #print(y, grid_y * 32, grid_y)
        #exit(1)

        train = Image(source=source, color=color, size_hint=(1, 1), pos=(x, y))
        self.layout.add_widget(train)
        
        animation = None
        self.trains.append((train, direction, animation))
        self.next_anim()

    def delete_trains(self, to_delete):
        self.trains = [(train, direction, animation) for train, direction, animation in self.trains if train not in to_delete]
        for train in to_delete:
            #print('DELETED train with color ' + str(train.color))
            self.layout.remove_widget(train)

    def calc_direction(self, train):
        new_dir_orientation = 0 if self.animation_map[train][0] != 0 else 1
        new_dir_direction = 1 if (self.animation_map[train][new_dir_orientation]) > 0 else 0
        new_dir = new_dir_orientation, new_dir_direction

        return new_dir

    def next_anim(self):
        #print('next_anim')
        for index, (train, direction, animation) in enumerate(self.trains):
            #print(index, ':', animation is None)
            if animation is None or animation.complete:
                #print('next_anim_real')
                grid_x = to_grid(train.pos[0], WIDTH) 
                grid_y = to_grid(train.pos[1], HEIGHT, -1) 
                #grid_x += WIDTH


                source, map_obj, direction = orientation_predicate(self.tile_map, grid_x, grid_y, lambda x, map_obj, direction: (os.path.join('../tiles/', x), map_obj, direction), lambda map_obj, direction: (None, map_obj, direction))
                #print('SOURCE2', source)
                #print(train.pos[0], grid_x * 32, grid_x)
                #print(train.pos[1], grid_y * 32, grid_y)
                if source:
                    train.source = source

                if train in self.animation_map:
                    # set entry dir
                    new_dir = self.calc_direction(train)
                else:
                    new_dir = direction
                vector_x, vector_y = movement_vector(map_obj, new_dir)
                if vector_x is not None:
                    #vector_x += grid_x
                    #vector_y += grid_y
                    self.animation_map[train] = (vector_x, vector_y)
                    
                    vector_x, vector_y = vector_convert(train, vector_x, vector_y)
                    
                    #print('orientation is:', str(map_obj), 'animating with', vector_x, vector_y)

                    #coll_trains = [(train, new_dir if i == index else direction, animation) for i, (train, direction, animation) in enumerate(self.trains)]
                    animation = TrainAnim(lambda: self.next_anim(), lambda x: self.delete_trains(x), lambda x, y: collision_check(self.trains,self.tile_map, x, y), x=vector_x, y=vector_y, duration=1/SPEED)
                else:
                    #dead code! BUG
                    raise Exception('BUGS!')
                    # keep old vector
                    #print('old vector', self.animation_map[train])
                    next_x, next_y = self.animation_map[train]
                    vector_x, vector_y = vector_convert(train, next_x, next_y)
                        
                    animation = TrainAnim(lambda: self.next_anim(), lambda x: self.delete_trains(x), lambda x, y: collision_check(self.trains, self.tile_map, x, y), x=vector_y, y=vector_y, duration=1/SPEED)
                animation.start(train)
                new_dir = self.calc_direction(train)
                self.trains[index] = train, new_dir, animation
        #anim = Animation(x=100, y=100, duration=5)
        #anim.start(train)

class LevelScreen(Screen):
    def __init__(self, lvl_nr, **kwargs):
        super(LevelScreen, self).__init__(**kwargs)
        self.name = "Level " + str(lvl_nr)
        lvl_tmx = '../lvl' + str(lvl_nr) + '.tmx'
        tile_map = data_structure.Map(WIDTH, HEIGHT, data_structure.make_trail_tile, lvl_tmx)       
        planet_field = data_structure.PlanetField(lvl_tmx, TILE_SIZE)
        train_starts = data_structure.TrainStarts(lvl_tmx, TILE_SIZE)
        layout = OwnFloatLayout(size=(WIDTH*TILE_SIZE, HEIGHT*TILE_SIZE))
        trains = Trains(layout, tile_map)


        #layout.add_widget(Image(source='../background.png', size_hint=(1, 1), pos=(-(WIDTH * TILE_SIZE / 2), HEIGHT * TILE_SIZE / 2)))
        

        planet_colors = [(1, 0, 0, .9), (0, 1, 0, .9), (0, 0, 1, .9), (1, 1, 0, .9), (0, 1, 1, .9), (1, 0, 1, .9)]
        for color, (x, y) in zip(planet_colors, planet_field.coords):
            #print(' PLANET: ', x, y)
            layout.add_widget(Image(source='../world.png', color=color, size_hint=(1, 1), pos=(x + 32, y - 32))) # TODO plus 32 hack
            #layout.add_widget(Image(source='../world.png', color=color, size_hint=(1, 1), pos=(x, y)))
        
        game_grid = GameGrid(tile_map, size_hint=(1, 1), pos=(0, 0))
        layout.add_widget(game_grid)

        #train = Image(source='../tiles/008.png', size_hint=(1, 1), pos=(0, 0))
        #layout.add_widget(train)

        for color, (x, y), (grid_x, grid_y) in zip(planet_colors, train_starts.coords, train_starts.grid_coords):
            trains.add(color, x, y, grid_x, grid_y)
        
        tile_map.set_train_station_colors(planet_colors)
        self.add_widget(layout) 

class WinScreen(Screen):
    def __init__(self, **kwargs):
        super(WinScreen, self).__init__(**kwargs)
        self.add_widget(Label(text="CONGRATULATIONS! All trains have found their way home!\nThanks for playing!!"))
        #Clock.schedule_once(lambda x: play_sound("win.wav"), 1)
        Clock.schedule_once(lambda x: exit(0), 5)
class GameOverScreen(Screen):
    def __init__(self, **kwargs):
        super(GameOverScreen, self).__init__(**kwargs)
        self.add_widget(Label(text="GAME OVER\nSeems the universe has imploded..."))
        Clock.schedule_once(lambda x: exit(0), 5)
class GameStateManager(ScreenManager):
    def __init__(self, nr_levels, **kwargs):
        super(GameStateManager, self).__init__(**kwargs)
        self.transition=FadeTransition()
        self.nr_levels = nr_levels
        self.nextlvl = 0
        self.bgm = None
    def next_level(self):
        if self.nextlvl < self.nr_levels - 1:
            self.nextlvl += 1
            play_sound("level_clear.wav")
            self.load_level()
        else:
            self.win_game()
    def load_level(self):
        time.sleep(0.5)
        next_level = LevelScreen(self.nextlvl)
        stop_sound(self.bgm)
        #self.remove_widget(self.current)
        self.switch_to(next_level)
        self.bgm = play_sound('train.mp3', loop=True)
    def win_game(self):
        print("you won the game!")
        self.next_screen("win.wav", WinScreen())
    def lose_level(self):
        print("you lost")
        #self.load_level()
        self.next_screen("game_over.wav", GameOverScreen())
    def next_screen(self, sound, screen):
        stop_sound(self.bgm)
        play_sound(sound)
        time.sleep(0.5)
        self.switch_to(screen)

NR_LEVELS = 3
gsm = None # ugly global hack needed because of Kivy widget positioning

def game_over():
    gsm.lose_level()
    pass

def win():
    gsm.next_level()
    pass

class SpaceTrainCrashApp(App):

    def build(self):
        global gsm
        gsm = GameStateManager(NR_LEVELS)
        #NR_LEVELS = 1
        #level = [LevelScreen(i) for i in range(NR_LEVELS)]
        #sm = ScreenManager()
        #sm.switch_to(level[0])
        #sm.add_widget(level[1])
        #sm.current = level[0].name
        gsm.load_level()
        return gsm


if __name__ == '__main__':
    Config.set('graphics', 'resizable', False)
    Config.set('graphics', 'width', str(TILE_SIZE * WIDTH))
    Config.set('graphics', 'height', str(TILE_SIZE * HEIGHT))
    SpaceTrainCrashApp().run()
