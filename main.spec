# -*- mode: python -*-

import glob

block_cipher = None


a = Analysis(['src/main.py'],
             pathex=['/home/ubuntu/space_train'],
             binaries=[],
             datas=[('.', file) for file in ['background.png', 'world.png'] + glob('*.tmx') + glob.glob('sounds/*') + glob.glob('tiles/*')],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          [],
          exclude_binaries=True,
          name='main',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          console=True )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               name='main')
